# MinimalPoster

*MinimalPoster* is a Latex package for creating, as the name suggests, minimal posters in Latex. The primary
motivation in creating the package was to make it *extremely* easy for Latex users to create 
modern looking posters in Latex with minimal visual noise and unnecessary flourish. 
Content is organized into *columns* and the only design choices involved in designing posters
using *MinimalPoster* are: the orientation of the poster, i.e., potrait or landscape, and 
the number of columns and the relative width of columns. Content in columns are
standard Latex constructs. The package is a thin wrapper around the a0poster Latex class and textpos package.

The following are two example posters created with the package.
![potrait](https://bitbucket.org/asish_geek/minimalposter/raw/master/_screenshots/potrait.png)
![landscape](https://bitbucket.org/asish_geek/minimalposter/raw/master/_screenshots/landscape.png)


# Getting Started
Let's say you want to create a three column poster in landscape mode with the column widths
being in the ratio 2:3:2. This can be accomplished by the creating 7 (2+3+2) *logical* columns
and then creating three *actual* columns with widths 2, 3 and 2 respectively.

    \documentclass[a0,final]{a0poster}
    \usepackage[columns=7]{minimalposter}

Now to create the actual columns, we do the following:

    \BeginColumn[2]
    ...
    \EndColumn

    \BeginColumn[3]
    ...
    \EndColumn

    \BeginColumn[2]
    ...
    \EndColumn

where the width of each column, in terms of number of *logical* columns, is specified in square braces.
Content inside each `\BeginColumn` and `\EndColumn` block can be organized into standard Latex sections.
For more complete examples checkout the examples provided with the package.


# Color Themes
The package provides the following pre-defined color themes: tranquil, zen, fury, greed, calm, dirty and fear.
Additional color themes can be created easily.


# Pre-requisites
Needless to say a working latex installation is needed for using the package. The package
also requires the following Latex packages: textpos, xcolor, tikz, kpfonts, kvoptions, calc, and sectsty.


# License
![license](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

The package itself is distributed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
The license does not apply to the posters created with the package.
